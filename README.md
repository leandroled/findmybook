# README #

Catálogo de seus livros e um controle para saber a quem que foi emprestado e quem já devolveu.

### Tecnologias  ###

* PHP 7
* PHPUnit
* MySQL
* Nginx
* AngularJS 1.x
* Docker
* Lumen
* Composer

